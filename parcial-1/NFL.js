// Instrucciones: Lee detalladamente el problema y realiza lo que se solicita de la forma correcta.

// La liga de la NFL requiere una aplicación en JS que resuelva el problema de la pizarra de pendientes,
// la aplicaciones debe contemplar el control de la lista de pendientes (ToDo),
// esta aplicacion debe de obtener la lista de pendientes de la siguiente url: http://jsonplaceholder/.typicode.com/todos
// Se recomienda visitar la url y comprobar que esta toda la información necesaria, por otro lado la NFL necesita que la aplicación realice lo siguiente:

// Lista de todos los pendientes (solo IDs) --
// Lista de todos los pendientes (IDs y Titles) --
// Lista de todos los pendientes sin resolver (ID y Title) --
// Lista de todos los pendientes resueltos (ID y Title) --
// Lista de todos los pendientes (IDs y userID) --
// Lista de todos los pendientes resueltos (ID y use/D) --
// Lista de todos los pendientes sin resolver (ID y useriD)
// La información oportuna y confiable le permite a la NFL tomar las mejores decisiones para la evolución de la liga.
// Motivo por el cual la NFL confía en que la aplicación contendrá un menú para un mejor manejo de la aplicación.

// Nota: Cualquier duda con la redacción del examen puedes consultarla con tu docente, el tiempo de entrega es el indicado por tu docente
// Nota2: Rúbrica en la siguiente página


const URL = 'http://jsonplaceholder.typicode.com/todos';


async function ListaPendientes1() {
  try {
    const respuesta = await fetch(URL);
    const datos = await respuesta.json();


    const listaIDs = datos.map(pendiente => pendiente.id);
    console.log("Lista de todos los pendientes (solo IDs):", listaIDs);


  } catch (error) {
    console.error("Error al obtener la lista de pendientes (solo IDs):", error);
  }
}

ListaPendientes1();


async function ListaPendientes2() {
    try {
      const respuesta = await fetch(URL);
      const datos = await respuesta.json();
  
  
      const listaIDsyTitles = datos.map(pendiente => ({ id: pendiente.id, title: pendiente.title }));
      console.log("Lista de todos los pendientes (solo IDs y Titles):", listaIDsyTitles);
  
  
    } catch (error) {
      console.error("Error al obtener la lista de pendientes (solo ID's y Titles):", error);
    }
  }
  
  ListaPendientes2();


async function ListaPendientes3() {
    try {
        const respuesta = await fetch(URL);
        const datos = await respuesta.json();

        const pendientesSinResolver = datos.filter(pendiente => !pendiente.completed);
        const listaSinResolver = pendientesSinResolver.map(pendiente => ({ id: pendiente.id, title: pendiente.title }));
        console.log("Lista de todos los pendientes sin resolver (solo ID's y Titles):", listaSinResolver);
    } catch (error) {
        console.error("Error al obtener la lista de los pendientes sin resolver (solo IDs):", error);
    }
}

ListaPendientes3();


async function ListaPendientes4() {
    try {
        const respuesta = await fetch(URL);
        const datos = await respuesta.json();

        const PendientesResueltos = datos.filter(pendiente => pendiente.completed);
        const ListaResueltos = PendientesResueltos.map(pendiente => ({ id: pendiente.id, title: pendiente.title }));
        console.log("Lista de todos los pendientes resueltos (solo ID's y Titles) :", ListaResueltos);
    } catch (error) {
        console.error("Error al obtener la lista de los pendientes resueltos (solo ID's y Titles): ", error);
    }
}

ListaPendientes4();


async function ListaPendientes5() {
    try {
        const respuesta = await fetch(URL);
        const datos = await respuesta.json();

        const listaIDsYUser = datos.map(pendiente => ({ id: pendiente.id, userId: pendiente.userId }));
        console.log("Lista de todos los pendientes (solo ID's y Users ID's): ", listaIDsYUser);
    } catch (error) {
        console.error("Error al obtener todos los pendientes (Solo ID's y Users ID's): ", error);
    }
}

ListaPendientes5();


async function ListaPendientes6() {
    try {
        const respuesta = await fetch(URL);
        const datos = await respuesta.json();

        const PendientesResueltos = datos.filter(pendiente => pendiente.completed);
        const ListaResueltos = PendientesResueltos.map(pendiente => ({ id: pendiente.id, userId: pendiente.userId }));
        console.log("Lista de todos los pendientes resueltos (solo ID's y Users ID's) :", ListaResueltos);
    } catch (error) {
        console.error("Error al obtener la lista de los pendientes resueltos (solo ID's y Users ID's): ", error);
    }
}

ListaPendientes6();


async function ListaPendientes7() {
    try {
        const respuesta = await fetch(URL);
        const datos = await respuesta.json();

        const pendientesSinResolver = datos.filter(pendiente => !pendiente.completed);
        const listaSinResolver = pendientesSinResolver.map(pendiente => ({ id: pendiente.id, userId: pendiente.userId }));
        console.log("Lista de todos los pendientes sin resolver (solo ID's y Users ID's):", listaSinResolver);
    } catch (error) {
        console.error("Error al obtener la lista de los pendientes sin resolver (solo IDs y Users ID's):", error);
    }
}

ListaPendientes7();

